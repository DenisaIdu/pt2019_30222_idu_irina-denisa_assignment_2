package Controller;

import Model.Client;
import Model.Scheduler;
import Model.Server;
import View.GUI;
import View.ViewSim;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class SimulationManager implements Runnable {
    private Scheduler scheduler;
    private ViewSim sim;

    private ArrayList<Client> clients;


    private int simTime;
    private int minServiceTime = 3;
    private int maxServiceTime = 14;
    private int nrOfQueues;
    private int minGenTime;
    private int maxGenTime;
    private int currentTime = 0;



    public SimulationManager(int simTime, int minServiceTime, int maxServiceTime, int nrOfQueues, int minGenTime, int maxGenTime) {
        clients = new ArrayList<Client>();
        this.maxGenTime = maxGenTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.minGenTime = minGenTime;
        this.nrOfQueues = nrOfQueues;
        this.simTime = simTime;

        sim = new ViewSim();

        this.scheduler = new Scheduler(nrOfQueues,sim);
    }

    public void run() {
        currentTime = 0;
        int timeBetweenClients = (int) (Math.random() * ((maxGenTime - minGenTime) + 1)) + minGenTime;
        int id = 0;
        while (currentTime < simTime * maxServiceTime*600) {
            if (currentTime == timeBetweenClients) {
                if (currentTime <= simTime) {
                    id++;
                    int prossTime = minServiceTime + (int) (Math.random() * ((maxServiceTime - minServiceTime) + 1));
                    Client c = new Client(id, currentTime, prossTime);
                    this.scheduler.dispatchClient(c);
                }
                timeBetweenClients = (int) (Math.random() * (maxGenTime - minGenTime + 1)) + minGenTime + currentTime;
            }


            sim.ShowLog(scheduler.getServers());

            currentTime++;

            try {
                Thread.sleep(10);
            } catch (Exception e) {

            }
        }
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

}

