package View;

import Controller.SimulationManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    private JLabel l1, l2, l3, l4;
    private JTextField tf1, tf2, tf31, tf32, tf41, tf42;
    private JButton b1;

    private int simTime;
    private int minServiceTime;
    private int maxServiceTime;
    private int nrOfQueues;
    private int minGenTime;
    private int maxGenTime;


    public GUI() {
        JFrame f = new JFrame();
        f.setLayout(null);

        f.setSize(400, 300);


        l1 = new JLabel("No of Servers: ");
        l2 = new JLabel("Simultation Time: ");
        l3 = new JLabel("Service Time interval: ");
        l4 = new JLabel("Generate clients interval: ");

        tf1 = new JTextField();
        tf2 = new JTextField();
        tf31 = new JTextField();
        tf32 = new JTextField();
        tf41 = new JTextField();
        tf42 = new JTextField();

        b1 = new JButton("Start");


        l1.setBounds(10, 20, 150, 30);
        l2.setBounds(10, 50, 150, 30);
        l3.setBounds(10, 90, 150, 30);
        l4.setBounds(10, 130, 150, 30);
        tf1.setBounds(160, 25, 150, 20);
        tf2.setBounds(160, 55, 150, 20);
        tf31.setBounds(160, 95, 70, 20);
        tf32.setBounds(240, 95, 70, 20);
        tf41.setBounds(160, 135, 70, 20);
        tf42.setBounds(240, 135, 70, 20);
        b1.setBounds(240, 170, 100, 20);



        f.add(l1);
        f.add(l2);
        f.add(l3);
        f.add(l4);
        f.add(tf1);
        f.add(tf2);
        f.add(tf31);
        f.add(tf32);
        f.add(tf41);
        f.add(tf42);
        f.add(b1);


        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    nrOfQueues=Integer.parseInt(getTf1());
                    simTime=Integer.parseInt(getTf2());
                    minServiceTime=Integer.parseInt(getTf31());
                    maxServiceTime=Integer.parseInt(getTf32());
                    minGenTime=Integer.parseInt(getTf41());
                    maxGenTime=Integer.parseInt(getTf42());

                }catch (Exception a){
                    a.printStackTrace();
                }

                SimulationManager s=new SimulationManager(simTime,minServiceTime,maxServiceTime,nrOfQueues,minGenTime,maxGenTime);
                Thread th=new Thread(s);
                th.start();

            }
        });

        f.setVisible(true);

    }


    public String getTf2() {
        return tf2.getText();
    }


    public String getTf1() {
        return tf1.getText();
    }

    public String getTf31() {
        return tf31.getText();
    }

    public String getTf32() {
        return tf32.getText();
    }


    public String getTf41() {
        return tf41.getText();
    }

    public String getTf42() {
        return tf42.getText();
    }



}
