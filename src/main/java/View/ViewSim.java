package View;

import Controller.SimulationManager;
import Model.Client;
import Model.Server;
import javax.swing.*;
import java.util.ArrayList;

public class ViewSim {
    private JTextArea ta2, ta3, ta4,ta1;
    private JScrollPane ts1, ts2, ts3, ts4;
    private JLabel ls1, ls2, ls3;
    private JLabel l1;
    private JTextArea tf1,tf2,tf3,tf4;

    private int peekT=0;


    public ViewSim() {
        JFrame f = new JFrame();
        f.setLayout(null);

        f.setSize(1100, 600);

        ta1 = new JTextArea();
        ts1 = new JScrollPane(ta1);
        ts1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        ts1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        ta2 = new JTextArea();
        ts2 = new JScrollPane(ta2);
        ts2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        ta3 = new JTextArea();
        ts3 = new JScrollPane(ta3);
        ts3.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        ta4 = new JTextArea();
        ts4 = new JScrollPane(ta4);
        ts4.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        ls1 = new JLabel("Server 1");
        ls2 = new JLabel("Server 2");
        ls3 = new JLabel("Server 3");

        l1=new JLabel("Peek time:");



        tf1=new JTextArea();
        tf2=new JTextArea();
        tf3=new JTextArea();
        tf4=new JTextArea();


        ts2.setBounds(20, 50, 160, 350);
        ts3.setBounds(220, 50, 160, 350);
        ts4.setBounds(440, 50, 160, 350);
        ls1.setBounds(50, 20, 100, 20);
        ls2.setBounds(250, 20, 100, 20);
        ls3.setBounds(470, 20, 100, 20);
        l1.setBounds(200,490,100,20);
        tf1.setBounds(300,490,100,20);
        tf2.setBounds(60,410,30,20);
        tf3.setBounds(260,410,30,20);
        tf4.setBounds(510,410,30,20);
        ts1.setBounds(630,50,400,300);


        f.add(ts2);
        f.add(ts3);
        f.add(ts4);

        f.add(ls1);
        f.add(ls2);
        f.add(ls3);

        f.add(l1);
        f.add(tf1);
        f.add(tf2);
        f.add(tf3);
        f.add(tf4);
        f.add(ts1);


        ts4.setVisible(false);
        ts2.setVisible(false);
        ts3.setVisible(false);
        ls1.setVisible(false);
        ls2.setVisible(false);
        ls3.setVisible(false);
        tf2.setVisible(false);
        tf3.setVisible(false);
        tf4.setVisible(false);


        f.setVisible(true);
    }

    public void Logger(String s){
        ta1.append(s);
    }

    public void ShowLog(ArrayList <Server> servers){
        int wt1=0;
        int wt2=0;
        int wt3=0;
        int peek=0;

        for (int i=0;i<servers.size();i++){
            if(i==0){
                ts2.setVisible(true);
                ls1.setVisible(true);
                tf2.setVisible(true);
                ta2.setText("");
                ArrayList<Client> clients=new ArrayList<Client>(servers.get(i).getClients());
                for (int j=0;j<clients.size();j++){
                    ta2.append("Client: "+clients.get(j).getId()+'\n');
                    peek++;
                    wt1+=clients.get(j).getTimpServiciu();
                }

            }

            if(i==1){
                ts3.setVisible(true);
                ls2.setVisible(true);
                tf3.setVisible(true);
                ta3.setText("");
                ArrayList<Client> clients=new ArrayList<Client>(servers.get(i).getClients());
                for (int j=0;j<clients.size();j++){
                    ta3.append("Client: "+clients.get(j).getId()+'\n');
                    wt2+=clients.get(j).getTimpServiciu();
                    peek++;

                }

            }

            if(i==2){
                ta4.setText("");
                ts4.setVisible(true);
                ls3.setVisible(true);
                tf4.setVisible(true);
                ArrayList<Client> clients=new ArrayList<Client>(servers.get(i).getClients());
                for (int j=0;j<clients.size();j++){
                    ta4.append("Client: "+clients.get(j).getId()+'\n');
                    wt3+=clients.get(j).getTimpServiciu();
                    peek++;
                }


            }
        }

        int wt=(wt1+wt2+wt3)/3;
        if(peekT<peek)
            peekT=peek;
        tf1.setText(String.valueOf(peekT));
        tf2.setText(String.valueOf(wt1));
        tf3.setText(String.valueOf(wt2));
        tf4.setText(String.valueOf(wt3));

    }
}
