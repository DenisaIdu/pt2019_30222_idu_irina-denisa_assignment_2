package Model;

public class Client {
    private int id;
    private  long oraSosire;
    private long timpFinalizare;
    private  long timpServiciu;
    private int server;

    public Client(int id,long oraSosire,long timpServiciu){
        this.id=id;
        this.oraSosire=oraSosire;
        this.timpServiciu=timpServiciu;
        this.server=0;
    }

    public int getServer() {
        return server;
    }

    public void setServer(int server) {
        this.server = server;
    }

    public int getId() {
        return id;
    }

    public long getOraSosire() {
        return oraSosire;
    }

    public long getTimpFinalizare() {
        return timpFinalizare;
    }

    public long getTimpServiciu() {
         return timpServiciu;
    }



    public void setOraSosire(long oraSosire) {
        this.oraSosire = oraSosire;
    }

    public void setTimpFinalizare(long timpFinalizare) {
        this.timpFinalizare = timpFinalizare;
    }
}
