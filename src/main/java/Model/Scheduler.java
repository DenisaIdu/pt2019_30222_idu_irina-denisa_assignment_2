package Model;

import View.ViewSim;

import java.util.ArrayList;

public class Scheduler{

    private ArrayList<Server> servers;
    private int nrCozi;
    private ViewSim viewSim;

    public Scheduler(int noQueue,ViewSim viewSim){
        this.viewSim=viewSim;

        servers=new ArrayList<Server>();
        for(int i=0;i<noQueue;i++)
        {
            Server s=new Server();
            s.setNoServer(i+1);
            Thread thread=new Thread(s);
            servers.add(s);
            thread.start();
        }
        //System.out.println("Nr de servere din array: "+servers.size());
    }

    public void dispatchClient(Client c){
        Server s=null;
        int minWT=99999;
        for(int i=0;i<servers.size();i++){
            if(servers.get(i).getwTime()<minWT && servers.get(i).getOpen()==1){
                minWT=servers.get(i).getwTime();
                s=servers.get(i);
            }
        }
        viewSim.Logger(toStringC(c,s.getNoServer()));
        c.setServer(s.getNoServer());
        s.addClient(c);
    }

    public String toStringC(Client c,int nrServer){
        return "Clientul" +c.getId()+"a sosit la "+c.getOraSosire()+"si are timpul de procesare:"+c.getTimpServiciu()+" la serverul "+nrServer+'\n';
    }

    public void  addServer(Server s){
        servers.add(s);
    }

    public ArrayList<Server> getServers() {
        return servers;
    }
}
