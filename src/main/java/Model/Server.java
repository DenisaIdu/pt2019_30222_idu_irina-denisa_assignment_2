package Model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class Server implements Runnable {
    private BlockingQueue<Client> clients;
    private int open;
    private int wTime;
    private int noClients;
    private int noServer;




    public Server(){
        this.open=1;
        clients=new LinkedBlockingQueue<Client>();
        this.noClients=0;
        this.wTime=0;
    }

    public void run(){
        while (true){
            try {
                if (clients.size()!=0){
                    Client client=clients.peek();
                    System.out.println("Clientul"+client.getId()+"este procesat la severul"+noServer);
                    Thread.sleep(client.getTimpServiciu()*250);
                    System.out.println("Clientul"+client.getId()+"a fost procesat");
                    this.wTime -= client.getTimpServiciu();
                    clients.take();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void addClient(Client c){
        try{
              clients.put(c);
              wTime+=c.getTimpServiciu();
              this.noClients++;
              System.out.println("Clientul "+c.getId()+"a sosit la "+c.getOraSosire()+"si are timpul de procesare:"+c.getTimpServiciu()+" la serverul "+noServer);
        } catch (Exception e){

      }
    }


    public BlockingQueue<Client> getClients() {
        return clients;
    }

    public int getwTime() {
        return wTime;
    }

    public int getOpen() {
        return open;
    }

    public int getNoClients() {
        return noClients;
    }

    public void setNoServer(int noServer) {
        this.noServer = noServer;
    }

    public int getNoServer() {
        return noServer;
    }
}